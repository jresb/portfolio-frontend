import {Fragment,useState} from 'react'
import {Jumbotron, Button, Card, Container, Row, Col, Form} from 'react-bootstrap'
import Image from 'next/image'
import Head from 'next/head'
import Swal from 'sweetalert2'
import moment from 'moment'

export default function Home() {

	moment.defaultFormat = "DD.MM.YYYY HH:mm"
	let time = new Date()

	const [fullName, setFullName] = useState('')
	const [email, setEmail] = useState('')
	const [message, setMessage] = useState('')
	const [mobileNo, setMobileNo] = useState('')
	const [date, setDate] = useState('')

	function contactDev(e) {
		e.preventDefault();

		console.log(fullName)
		console.log(email)
		console.log(message)
		console.log(mobileNo)

		fetch('https://protected-dusk-46822.herokuapp.com/api/users', {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				fullName: fullName,
				email: email,
				message: message,
				mobileNo: mobileNo
				// date: new Date().toLocaleString()
				// date: moment(time, moment.defaultFormat).toDate()
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			setFullName('')
			setEmail('')
			setMessage('')
			setMobileNo('')

			if (data) {
				Swal.fire({
					icon: "success",
					background: '#FFFFFF',
					title: "Message sent to the developer.",
					text: `We'll get back to you soon!`
				})
			} else {
				Swal.fire({
					icon: "success",
					background: '#FFFFFF',
					title: "Message sending failed.",
					text: `Something went wrong. You can opt to send a message the developer via his contact details instead.`
				})
			}
		})
	}

	return (
		<Jumbotron fluid id="contact-jumbo">
		    <Container>
			    <Head>
			    	<title>Jerico Ballesteros | Web Developer</title>
			    </Head>
			    <h1 className="text-white">Contact the Developer</h1>
			    <Row className ="justify-content-center">
			    	<Col xs={12} sm={12} md={8} lg={8}>
			    		<Card className="bg-dark text-white my-3 mx-auto h-100">
							<Card.Body>
								<Card.Title className="text-center" style={{fontSize: 30}}></Card.Title>
									<Form onSubmit={e => contactDev(e)}>
										<Row>
											<Col xs={12} sm={12} md={6} lg={6}>
								                <Form.Group controlId="fullName">
								                    <Form.Label>Full Name:</Form.Label>
								                    <Form.Control type="text" placeholder="Enter your Full Name" value={fullName} onChange={e => setFullName(e.target.value)} required />
								                </Form.Group>

								                <Form.Group controlId="email">
								                    <Form.Label>Email:</Form.Label>
								                    <Form.Control type="email" placeholder="Enter your Email" value={email} onChange={e => setEmail(e.target.value)} required />
								                </Form.Group>

								                <Form.Group controlId="mobileNo">
								                    <Form.Label>Mobile Number:</Form.Label>
								                    <Form.Control type="text" placeholder="Enter your Mobile Number" value={mobileNo} onChange={e => setMobileNo(e.target.value)} />
								                </Form.Group>
											</Col>

											<Col xs={12} sm={12} md={6} lg={6}>
								                <Form.Group controlId="message">
								                    <Form.Label>Message:</Form.Label>
								                    <Form.Control as="textarea" rows={8} placeholder="Enter your Message" value={message} onChange={e => setMessage(e.target.value)} required />
								                </Form.Group>
											</Col>
										</Row>
				                		<Button id="send-button" type="submit" className="btn my-2 btn-block">Submit</Button>
						            </Form>
							</Card.Body>
						</Card>
			    	</Col>
			    </Row>
		    </Container>
	    </Jumbotron>
	)
}