import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/globals.css'
import {useState,useEffect,Fragment} from 'react'
import Head from 'next/head'

// import components
import NavBar from '../components/NavBar'
import Landing from '../components/Landing'
import Portfolio from '../components/Portfolio'
import About from '../components/About'
import Footer from '../components/Footer'
// import BS components
import {Container} from 'react-bootstrap'

function MyApp({ Component, pageProps }) {

  return (
  	<Fragment>
	  	<Head>
	  		<link rel="preconnect" href="https://fonts.gstatic.com" />
  			<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200&family=Quicksand:wght@300&display=swap" rel="stylesheet" />
	  		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossOrigin="anonymous" />
	  	</Head>
	  		<NavBar />
	  		<Landing />
	  		<Portfolio />
			<Component {...pageProps} />
			<About />
			<Footer />
  	</Fragment>
  	)
}

export default MyApp
