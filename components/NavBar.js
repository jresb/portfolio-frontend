import {useState, useContext, Fragment} from 'react'
import {Navbar,Nav} from 'react-bootstrap'

// import Link from 'next/link'
import {Link} from 'react-scroll'

export default function NavBar() {

	return (
		<Navbar bg="dark" variant="dark" expand="lg" className="fixed-top">
		  <Navbar.Brand href="/">Jerico</Navbar.Brand>
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
			<Nav className="mr-auto">
				<Link className="nav-link" smooth={true} duration={1000} to="lead-jumbo">Home</Link>
				<Link className="nav-link" smooth={true} duration={1000} to="portfolio-jumbo">Projects</Link>
				<Link className="nav-link" smooth={true} duration={1000} to="contact-jumbo">Contact</Link>
			    <Link className="nav-link" smooth={true} duration={1000} to="about-jumbo">About</Link>
			</Nav>
		  </Navbar.Collapse>
		</Navbar>
	)
}