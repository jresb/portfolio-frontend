import {Jumbotron, Button, Accordion, Card, Container, Row, Col, Form} from 'react-bootstrap'
import Image from 'next/image'

export default function Portfolio() {
	return (
		<Jumbotron fluid id="portfolio-jumbo">
			<Container>
				<h1>Projects</h1>
			    <Accordion defaultActiveKey="0">
				  	<Card className="bg-dark text-white" id="cap1-card">
				    	<Card.Header>
				      		<Accordion.Toggle as={Button} variant="link" eventKey="0" className="text-warning" id="accordToggle">
				        	Capstone 1 (Online Portfolio)
				      		</Accordion.Toggle>
				    	</Card.Header>
				    	<Accordion.Collapse eventKey="0">
				      		<Card.Body>
				      		<Row>
				      			<Col xs={12} sm={12} md={6} lg={6} className="text-center">
						      		<Image className="mx-auto"
										src="/no-1.png"
										alt="cap3-png"
										width={458 * 0.8}
										height={458 * 0.8}
									/>
				      			</Col>
				      			<Col xs={12} sm={12} md={6} lg={6}>
						      		<p>
						      			My online portfolio made with HTML, CSS, and Bootstrap. This actual page is a refurbished version of my first capstone using JS. To see the simpler HTML version, click View.
						      		</p>
							      	<a href="https://jresb.gitlab.io/capstone1-ballesteros/" className="btn btn-block" id="view-button" target="_blank">View</a>
				      			</Col>
				      		</Row>
				      		</Card.Body>
				    	</Accordion.Collapse>
				  	</Card>
				  	<Card className="bg-dark text-white">
				    	<Card.Header>
				      		<Accordion.Toggle as={Button} variant="link" eventKey="1" className="text-warning" id="accordToggle">
				        	Capstone 2 (Peruse Booking System)
				      		</Accordion.Toggle>
				    	</Card.Header>
				    	<Accordion.Collapse eventKey="1">
				      		<Card.Body>
				      		<Row>
				      			<Col xs={12} sm={12} md={6} lg={6} className="text-center">
						      		<Image className="mx-auto"
										src="/no-2.png"
										alt="cap2-png"
										width={458 * 0.8}
										height={458 * 0.8}
									/>
				      			</Col>
				      			<Col xs={12} sm={12} md={6} lg={6}>
						      		<p>
					      				A Heroku and MongoDB hosted booking service web application made using Javascript and MEN Stack (MongoDB, Express, and Node). Backend application was created with Express and Node, database with MongoDB.
				      				</p>
						      		<a href="https://jresb.gitlab.io/capstone2-ballesteros" className="btn btn-block" id="view-button" target="_blank">View</a>
				      			</Col>
				      		</Row>
				      		</Card.Body>
				    	</Accordion.Collapse>
				  	</Card>
				  	<Card className="bg-dark text-white">
				    	<Card.Header>
				      		<Accordion.Toggle as={Button} variant="link" eventKey="2" className="text-warning" id="accordToggle">
				        	Capstone 3 (Pence Budget Web Application)
				      		</Accordion.Toggle>
				    	</Card.Header>
				    	<Accordion.Collapse eventKey="2">
				      		<Card.Body>
				      		<Row>
				      			<Col xs={12} sm={12} md={6} lg={6} className="text-center">
						      		<Image className="mx-auto"
										src="/no-3.png"
										alt="cap3-png"
										width={458 * 0.8}
										height={458 * 0.8}
									/>
				      			</Col>
				      			<Col xs={12} sm={12} md={6} lg={6}>
						      		<p>
							      		A budget tracking web application made with Next.js encompassed with MERN Stack. Frontend hosted with Vercel, backend with Heroku. Also includes Google Login!
						      		</p>
						    		<a href="https://capstone-3-client-ballesteros.vercel.app/" className="btn btn-block" id="view-button" target="_blank">View</a>
				      			</Col>
				      		</Row>
				      		
				      		</Card.Body>
				    	</Accordion.Collapse>
				  	</Card>
				</Accordion>
			</Container>
		</Jumbotron>
	)
}