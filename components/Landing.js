import {Jumbotron, Button, Card, Container, Row, Col, Form} from 'react-bootstrap'
import {Link} from 'react-scroll'

export default function Landing(){
	return (
		<Jumbotron fluid id="lead-jumbo">
			<Container>
				<h1 className="display-4">Hello, World!</h1>
			    <p>
			    	Hi! I'm Jerico, a Full-Stack Web Developer. Welcome to my profile!
			    </p>
			    <Link className="btn btn-lg" role="button" id="first-button" smooth={true} duration={1000} to="about-jumbo">Get to know me better ↓</Link>
			    {/*
			    <a className="btn btn-lg" id="first-button" href="#portfolio" role="button">Get to know me better ↓</a>
			    */}
			</Container>
		</Jumbotron>
	)
}
