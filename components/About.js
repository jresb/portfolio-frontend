import {Jumbotron, Container} from 'react-bootstrap'
import Image from 'next/image'
export default function About() {
	return (
		<Jumbotron fluid id="about-jumbo">
			<Container>
				<h1>About</h1>

			    <Image className="mx-auto"
					src="/resume1234_transparent.png"
					alt="res1-png"
					width={2000}
					height={3049}
				/>
			</Container>
		</Jumbotron>
	)
}